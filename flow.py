import prefect
from prefect import Flow, Parameter, task
from prefect.schedules.clocks import CronClock
from prefect.schedules import Schedule
from prefect.storage import GitLab

@task
def output_param(param):
    logger = prefect.context.get("logger")
    logger.info(param)
    logger.info(type(param))
    return param

with Flow("test-none-param") as flow:
    input_param = Parameter("input", default="value")

    output_param(input_param)

if __name__ == "__main__":
    # Configure schedule
    schedule = Schedule(clocks=[CronClock("*/5 * * * *")])
    flow.schedule = schedule

    # Configure storage
    flow.storage = GitLab(repo="andrei.nasonov/prefect-minimal-flow", path="flow.py", ref="main")

    # Set defaults to empty dictionary
    for param in flow.parameters():
        param.default = {}

    # Register
    flow.register(project_name="testing", label="axyltop")

